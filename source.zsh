# --- sourcing package aliases ---
# aliases are sourced as file fragments
d=/usr/local/etc/alias.d
for f in $d/*.alias.sh
do
source $f
done
# +-- sourcing package aliases ---

# --- sourcing zsh-specific aliases ---
# global and suffix aliases, namely
d=/usr/local/etc/alias.d
for f in $d/*.alias.zsh
do
source $f
done
# +-- sourcing zsh-specific aliases ---

# --- sourcing environment variables ---
d=/usr/local/etc/env.d
for f in $d/*.env.sh
do
source $f
done
# +-- sourcing environment variables ---

# --- sourcing zsh-specific options ---
d=/usr/local/etc/opt.d
for f in $d/*.opt.zsh
do
source $f
done	 
# +-- sourcing zsh-specific options ---
